public class SearchMovie {
    
    @AuraEnabled
    public static   Map<String,Object> getMovie(String movieSearched){
        String endpoint='http://www.omdbapi.com/';
        String myKey='38aacd81';
        
		//treating the string for search
		movieSearched=movieSearched.replace(' ', '+');
               
        endpoint += '?t='+ movieSearched;
        endpoint += '&apikey='+myKey;
        
        Http http=new Http();
        HttpRequest request = new HttpRequest();
        
        request.setMethod('GET');
        request.setEndpoint(endpoint);
        
        HttpResponse response= http.send(request);
        
        //setting up an map object to return
        Map<String,Object> movieFound = new Map<String,Object>();
        
        if (response.getStatus()=='OK'){
           movieFound = (Map<String,Object>)JSON.deserializeUntyped(response.getBody());
           
        }
        	else{
            //maybe put some code here    
            } 
        return movieFound;
    }
    
    @AuraEnabled
    public static void saveMovie(string mTitle, string mYear, string mDirector, string mGenre, string mImdbId,
                                 string mLanguage, string mRated, string mRuntime){
                                     list<Movie__c> checkMovie =[SELECT IMDB_id__c FROM Movie__c WHERE IMDB_id__c=:mImdbId];
                                     
                                     
                                     if(checkMovie.isEmpty()){
                                     
                                     Movie__c myMovie= new Movie__c();
                                         myMovie.Name=mTitle;
                                         myMovie.Year__c=mYear;
                                         myMovie.Director__c=mDirector;
                                         myMovie.Genra__c=mGenre;
                                         myMovie.IMDB_id__c=mImdbId;
                                         myMovie.Language__c=mLanguage;
                                         myMovie.Rated__c=mRated;
                                        myMovie.Runtime__c=mRuntime;
                                     
                                     upsert myMovie;
                                     }
                                     }
                                 
                                 
}