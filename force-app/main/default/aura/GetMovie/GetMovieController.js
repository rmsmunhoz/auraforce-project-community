({
	goMovie : function(component, event, helper) {
       //setting up the function 
        let goTitle = component.get('c.getMovie');
        let mt =component.get('v.movieToSearch');
        
        //setting parameters
        goTitle.setParams({movieSearched:mt});
        goTitle.setCallback(this,function(resp){
            
            let state = resp.getState();
            console.log(state);
            JSON.stringify(resp.getReturnValue());
            let cd = resp.getReturnValue();
            console.log(cd.Error);
            if(state=="SUCCESS"){
               component.set('v.show',"True");
                component.set('v.movie',cd);
                component.set('v.movieNotFound',"False");
                let b = component.get('v.movieNotFound');
                console.log(b);
                let a = component.get("v.show");
            } 
            if(cd.Error=="Movie not found!"){
                component.set('v.show',"False");
                component.set('v.movieNotFound',"True");
                component.set('v.movie.Poster',"https://wise-bear-gnb2kq-dev-ed--c.visualforce.com/resource/1603816122000/gatinhoSad");
            let b = component.get('v.movie.Title');
                console.log(b);    
            }
            
        })                            
        
        
	$A.enqueueAction(goTitle);	
	},
    chooseMovie: function(component, event, helper){
    //get the info collected from rest api to send it to apex controller
    let title = component.get('v.movie.Title');
    let year = component.get('v.movie.Year');
    let director = component.get('v.movie.Director');
    let genre = component.get('v.movie.Genre');
    let imdbId = component.get('v.movie.imdbID');
    let language = component.get('v.movie.Language');
    let rated = component.get('v.movie.Rated');
    let runtime = component.get('v.movie.Runtime');
   
    //get the function from apex
    let goChoice= component.get('c.saveMovie');
        
        //setting parameters
        goChoice.setParams({mTitle:title, mYear:year, mDirector:director, mGenre:genre, mImdbId:imdbId, mLanguage:language, mRated:rated, mRuntime:runtime});
        goChoice.setCallback(this, function(response){
            alert("Movie added to the catalog!"); 
            //do something.. or not
        }
        )                     

        $A.enqueueAction(goChoice);
		}
})