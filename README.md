Auraforce – Project Community

Auraforce is a custom project that leverages lightning communities, process automation, and the lightning aura framework to create an interactive experience that consumes an external rest API. In this project I created a fake movie theater company where customers can rent a movie theater room for a private experience. The customer can access the company community and  choose any movie, new or old, and book a reservation. 

Features:
•	Community auto sign-in for customers.
•	Editing account information.
•	Search for a movie and retrieve information from OMDB API, including:
-	Movie Title
-	Year
-	Language
-	Genre
-	Duration
-	Director
-	Actors
-	Plot
-	IMDB Id
-	IMDB rating
-	Poster
•	Add movie to the catalog
•	Book Experience
•	Check orders

Technologies:

-	Aura framework
-	Screen Flows
-	Process builder
-	SOQL
-	Apex controller
-	JavaScript
-	Aura components
-	CSS
-	Workflow Rules


# Salesforce DX Project: Next Steps

Now that you’ve created a Salesforce DX project, what’s next? Here are some documentation resources to get you started.

## How Do You Plan to Deploy Your Changes?

Do you want to deploy a set of changes, or create a self-contained application? Choose a [development model](https://developer.salesforce.com/tools/vscode/en/user-guide/development-models).

## Configure Your Salesforce DX Project

The `sfdx-project.json` file contains useful configuration information for your project. See [Salesforce DX Project Configuration](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_ws_config.htm) in the _Salesforce DX Developer Guide_ for details about this file.

## Read All About It

- [Salesforce Extensions Documentation](https://developer.salesforce.com/tools/vscode/)
- [Salesforce CLI Setup Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_setup.meta/sfdx_setup/sfdx_setup_intro.htm)
- [Salesforce DX Developer Guide](https://developer.salesforce.com/docs/atlas.en-us.sfdx_dev.meta/sfdx_dev/sfdx_dev_intro.htm)
- [Salesforce CLI Command Reference](https://developer.salesforce.com/docs/atlas.en-us.sfdx_cli_reference.meta/sfdx_cli_reference/cli_reference.htm)
